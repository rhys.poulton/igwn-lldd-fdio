# IGWN - Low Latency Data Distribution - Frame distribution Input/ Output

The IGWN - Low Latency Data Distribution (lldd) - Frame distribtion Input/ Output (FdIO) is software to distribute low latency data used by the International Gravitational-Wave Observatory Network (IGWN) using a FdIOServer.  

This software currently utilizes the [Apache Kafka](https://kafka.apache.org/) event streaming platform to distribute the low latency data by connecting to Kafka Broker(s) to produce or consume Kafka messages containing the data.
