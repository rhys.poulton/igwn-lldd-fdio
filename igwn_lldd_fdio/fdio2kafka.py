#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (2022)
#
# Author: Rhys Poulton <poulton@ego-gw.it>
#
# This file is part of igwn-lldd.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd.  If not, see <http://www.gnu.org/licenses/>.

import sys
import configargparse
import virgotools as vt
import PyFd as fd
import logging
from .utils import str2bool
from .framekafkaproducer import FrameKafkaProducer

logger = logging.getLogger(__name__)
# right now we're having trouble having these logs go to the pipes
# we set up before: we get a bunch of errors in the logging module
# when it tries to call self.stream.flush(), probably because of
# the O_NONBLOCKING option used when opened
# consoleHandler = logging.StreamHandler(f_log.get_file())
consoleHandler = logging.StreamHandler()
logger.addHandler(consoleHandler)


def frame_to_buffer(frame):
    # Get the estimated size of the frame
    f = fd.tmpfile()
    buffSize = int(1.1 * fd.FrameStat(frame, f) + 10000)
    fd.fclose(f)

    # Create the buffer
    buff = bytes(" " * buffSize, "utf-8")

    # Write into the buffer
    nbyteswritten = fd.FrameWriteToBuf(frame, 0, buff, buffSize, 0)

    if nbyteswritten > 0:
        return [0, buff[:nbyteswritten], int(frame.contents.GTimeS)]
    else:
        return [
            nbyteswritten,
            buff[:nbyteswritten],
            int(frame.contents.GTimeS),
        ]


def main():

    parser = configargparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        is_config_file=True,
        required=True,
        help="config file path",
    )
    parser.add_argument(
        "-d",
        "--debug",
        type=int,
        default=0,
        help="increase debug level (default 0=off)",
    )
    parser.add_argument(
        "-b",
        "--bootstrap-servers",
        type=str,
        default="localhost:9092",
        help="Specify the Kafka cluster bootstrap servers",
    )
    parser.add_argument("-l", "--log", type=str, help="Standard log file")
    parser.add_argument(
        "-nl",
        "--no-log",
        action="store_const",
        const=True,
        default=False,
        help="no standard log",
    )
    parser.add_argument("-v", "--verbose", type=str, help="Verbose log file")
    parser.add_argument(
        "-nv",
        "--no-verbose",
        action="store_const",
        const=True,
        default=False,
        help="no verbose log",
    )
    parser.add_argument("-g", "--group-id", type=str, help="Kafka group ID")
    parser.add_argument(
        "-S",
        "--split-bytes",
        type=int,
        default=100000,
        help="Split messages into this many bytes when adding to Kafka",
    )
    parser.add_argument(
        "-t", "--topic", type=str, help="Kafka topic to write to"
    )
    parser.add_argument(
        "-crc",
        "--crc-check",
        type=str2bool,
        default=True,
        help="Run a CRC check for each frame",
    )
    parser.add_argument(
        "-bs",
        "--batch-size",
        type=int,
        help="Change the batch.size for the producer [default: 16384]",
    )
    parser.add_argument(
        "-bm",
        "--buffer-memory",
        type=int,
        help="Change the buffer.memory for the producer \
[default: 33554432B=32MB]",
    )
    parser.add_argument(
        "-lm",
        "--linger-ms",
        type=int,
        help="Change the linger.ms for the producer [default: 0]",
    )
    parser.add_argument(
        "-a",
        "--acks",
        default=1,
        type=int,
        help="Change the acks for the producer",
    )
    parser.add_argument(
        "-mi",
        "--min-interval",
        default=-1,
        type=float,
        help="Enforce a minimum interval between gwf files",
    )
    parser.add_argument(
        "-su",
        "--status-updates",
        action="store_const",
        const=True,
        default=False,
        help="store status updates",
    )
    parser.add_argument(
        "-st", "--status-topic", type=str, help="topic name for status updates"
    )
    parser.add_argument(
        "-sb",
        "--status-bootstrap",
        type=str,
        help="specify the kafka cluster for status updates",
    )
    parser.add_argument(
        "-si",
        "--status-interval",
        type=int,
        default=60,
        help="interval in seconds between status updates",
    )
    parser.add_argument(
        "-dt",
        "--delta-t",
        default=1,
        type=int,
        help="Make sure each frame comes in at delta_t seconds",
    )
    # custom libraries
    parser.add_argument(
        "-lkp",
        "--load-kafka-python",
        action="store_const",
        const=True,
        default=False,
        help="load kafka-python rather than confluent-kafka",
    )

    # Parse the arguments from the command line
    args, unknown = parser.parse_known_args()

    # Put the logging into the expected format
    vt.log_to_cfg(level=logging.INFO)

    # Start the fdio server
    fdio = vt.FdIO(["", args.config])

    # Mark the server as configured
    fd.CfgReachState(fd.CfgServerConfigured)

    logger.info(f"verbose: [{args.verbose}]")
    logger.info(f"topic: [{args.topic}]")
    logger.info(f"crc_check: [{args.crc_check}]")
    logger.info(f"bootstrap_servers: [{args.bootstrap_servers}]")
    logger.info(f"split_bytes: [{args.split_bytes}]")
    logger.info(f"batch_size: [{args.batch_size}]")
    logger.info(f"linger_ms: [{args.linger_ms}]")
    logger.info(f"min_interval: [{args.min_interval}]")
    logger.info(f"delta_t: [{args.delta_t}]")

    framekafkaproducer = FrameKafkaProducer(args)

    # Mark the server as golden
    fd.CfgReachState(fd.CfgServerGolden)

    last_ret = 0
    while last_ret == 0 and not fd.CfgFinished():
        frame = fd.FdIOGetFrame(fdio._fdio)

        if not frame:
            continue

        [last_ret, data, timestamp] = frame_to_buffer(frame)

        # if args.crc_check:
        #     returncode = check_crc(reader, args.topic, data, timestamp)

        #     if returncode != 0:
        #         continue

        framekafkaproducer.send_frame(data, timestamp)

        # Keep the FDIoServer Alive
        if fdio._fdio.contents.userInfo is None:
            fd.CfgMsgAddUserInfo(
                "gps:%i file timestamp %d latency:%0.3f unix:%0.3f \
send:%0.3f flush:%0.3f send+flush:%0.3f avg_rate_30s_MBs:%0.3f data_n:%d\n"
                % (
                    framekafkaproducer.time_after_flush_gps,
                    timestamp,
                    framekafkaproducer.time_after_flush_gps - timestamp,
                    framekafkaproducer.time_after_flush_unix,
                    framekafkaproducer.time_before_flush_unix
                    - framekafkaproducer.time_before_send_unix,
                    framekafkaproducer.time_after_flush_unix
                    - framekafkaproducer.time_before_flush_unix,
                    framekafkaproducer.time_during_send_flush,
                    framekafkaproducer.data_rate,
                    framekafkaproducer.data_n,
                )
            )
            fd.CfgMsgSendWithTimeout(0.001)

    # close
    framekafkaproducer.close()


if __name__ == "__main__":
    sys.exit(main())
