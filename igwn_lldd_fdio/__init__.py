# -*- coding: utf-8 -*-
# Copyright (C) European Gravitational Observatory (2022)
#
# Author list: Rhys Poulton <poulton@ego-gw.it>
#
# This file is part of igwn-lldd-fdio.
#
# igwn-lldd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# igwn-lldd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with igwn-lldd-fdio.  If not, see <http://www.gnu.org/licenses/>.
__version__ = '0.1.0'
